//  Copyright 2024 Kai Oezer

import Testing
@testable import DEGAAP

@Suite("Linkbase tests", .tags(.linkbase))
struct LinkbaseTests
{
	@Test("CalculationArc initialization")
	func calculationArcInitialization() throws
	{
		#expect(CalculationArc(to: "@!+++", weight: .additive) == nil)
		let itemID = try #require(DEGAAPItemID("some.account"))
		let arc1 = try #require(CalculationArc(to: "some.account", weight: .additive))
		#expect(arc1.itemID == itemID)
		let arc2 = CalculationArc(to: itemID, weight: .subtractive)
		#expect(arc2.weight == .subtractive)
		#expect(arc2.itemID == itemID)
	}

	@Test("paths")
	func paths() throws
	{
#if CREATE_LINKBASE_FROM_KERNTAXONOMIE
		let linkbase1 = TILinkbase(taxonomyItems: _items1, reportSection: .balanceSheet)
		let linkbase2 = TILinkbase(taxonomyItems: _items2, reportSection: .balanceSheet)
		let linkbase3 = linkbase1.joining(with: linkbase2)
		#expect(linkbase3.path(for: DEGAAPItemID("bs.test.abc.def")!).count == 2)
		#expect(linkbase3.path(for: DEGAAPItemID("bs.test.xyz.uvw")!).count == 3)
		#expect(linkbase3.path(for: DEGAAPItemID("bs.test")!).isEmpty)
		#expect(linkbase3.path(for: DEGAAPItemID("bla.bla")!).isEmpty)
#endif
	}

	@Test("joining")
	func joining() throws
	{
#if CREATE_LINKBASE_FROM_KERNTAXONOMIE
		let linkbase1 = TILinkbase(taxonomyItems: _items1, reportSection: .balanceSheet)
		let linkbase2 = TILinkbase(taxonomyItems: _items2, reportSection: .balanceSheet)
		#expect(linkbase1.joining(with: linkbase2) == linkbase2.joining(with: linkbase1))
		let nonTILinkbase = SomeNonTILinkbase()
		#expect(linkbase1.joining(with: nonTILinkbase) == linkbase1)
#endif
	}

	@Test("weights")
	func weights() throws
	{
#if CREATE_LINKBASE_FROM_KERNTAXONOMIE
		let linkbase1 = TILinkbase(taxonomyItems: _items1, reportSection: .balanceSheet)
		let weights = linkbase1.weights(for: DEGAAPItemID("bs.test.abc")!, startingFrom: [DEGAAPItemID("bs.test.abc.def")!])
		#expect(weights.count == 1)
		#expect(weights.first!.rawValue == 1)

		let linkbase2 = TILinkbase(taxonomyItems: _items2, reportSection: .balanceSheet)
		let weights2 = linkbase2.weights(for: DEGAAPItemID("bs.test")!, startingFrom: [DEGAAPItemID("bs.test.xyz.uvw")!])
		#expect(weights2.count == 1)
		#expect(weights2.first!.rawValue == 1)

		let linkbase3 = TILinkbase(taxonomyItems: _items3, reportSection: .balanceSheet)
		let weights3 = linkbase3.weights(for: DEGAAPItemID("bs.test")!, startingFrom: [DEGAAPItemID("bs.test.mno.stu")!])
		#expect(weights3.count == 1)
		#expect(weights3.first!.rawValue == -1)
#endif
	}

	private var _items1 : [TaxonomyItem]
	{
		[
			TaxonomyItem(abstract: .tiFalse, balanceType: .debit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test.abc")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: .none, level: 3, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse),
			TaxonomyItem(abstract: .tiFalse, balanceType: .debit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test.abc.def")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: TaxonomyItem.TIParentItem("bs.test.abc"), level: 4, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse)
		]
	}

	private var _items2 : [TaxonomyItem]
	{
		[
			TaxonomyItem(abstract: .tiFalse, balanceType: .credit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: .none, level: 2, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse),
			TaxonomyItem(abstract: .tiFalse, balanceType: .credit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test.xyz")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: TaxonomyItem.TIParentItem("bs.test"), level: 3, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse),
			TaxonomyItem(abstract: .tiFalse, balanceType: .credit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test.xyz.uvw")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: TaxonomyItem.TIParentItem("bs.test.xyz"), level: 4, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse)
		]
	}

	private var _items3 : [TaxonomyItem]
	{
		[
			TaxonomyItem(abstract: .tiFalse, balanceType: .debit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: .none, level: 2, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse),
			TaxonomyItem(abstract: .tiFalse, balanceType: .debit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test.mno")!, reportSection: .balanceSheet, calcWeight: .minus, calcParent: TaxonomyItem.TIParentItem("bs.test"), level: 3, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse),
			TaxonomyItem(abstract: .tiFalse, balanceType: .debit, periodType: .none, valueType: .money, id: DEGAAPItemID("bs.test.mno.stu")!, reportSection: .balanceSheet, calcWeight: .plus, calcParent: TaxonomyItem.TIParentItem("bs.test.mno"), level: 4, definitionDE: "lange Beschreibung", definitionEN: "long description", shortDefinitionDE: "kurz", shortDefinitionEN: "short", documentation: "", usage: "", lawShortName: "HGB 1", lawParagraph: "3", lawSubparagraph: "2", notFor: .none, requirement: .requiredSummation, resultType: .gkv, legalFormEU: .tiTrue, legalFormPG: .tiFalse, legalFormKSt: .tiFalse)
		]
	}

}

struct SomeNonTILinkbase : DEGAAP.Linkbase
{
	var reverse: any DEGAAP.Linkbase { SomeNonTILinkbase() }

	func joining(with linkbase: any DEGAAP.Linkbase) -> any DEGAAP.Linkbase
	{
		return self
	}

	func weights(for account: DEGAAP.DEGAAPItemID, startingFrom startAccounts: [DEGAAP.DEGAAPItemID]) -> [DEGAAP.DEGAAPWeight]
	{
		[]
	}
	
	func path(for account: DEGAAP.DEGAAPItemID) -> [DEGAAP.DEGAAPItemID]
	{
		[]
	}
}
