// Copyright 2023-2025 Kai Oezer

import Foundation
import Testing
@testable import DEGAAP

@Suite("DEGAAPTaxonomyVersion tests", .tags(.xbrlTaxonomy))
struct TaxonomyTests
{
	@Test("initialization from date", arguments: [
		(2023, 4, DEGAAPTaxonomyVersion.v6_6),
		(2022, 2, DEGAAPTaxonomyVersion.v6_5),
		(2055, 1, nil),
	])
	func initializationFromDate(year : Int, month : Int, expectedVersion : DEGAAPTaxonomyVersion?) throws
	{
		let components = DateComponents(calendar: Calendar(identifier: .gregorian), year: year, month: month)
		let date = try #require(components.date)
		#expect(DEGAAPTaxonomyVersion(date: date) == expectedVersion)
	}

	@Test("human-readable representation")
	func humanReadableRepresenation()
	{
		#expect(DEGAAPTaxonomyVersion.v6_4.humanReadable == "6.4")
		#expect(DEGAAPTaxonomyVersion.v6_8.humanReadable == "6.8")
		#expect(DEGAAPTaxonomyVersion(fromHumanReadable: "6.5") == .v6_5)
		#expect(DEGAAPTaxonomyVersion(fromHumanReadable: "12.9") == nil)
		#expect(DEGAAPTaxonomyVersion(fromHumanReadable: "bla.bla") == nil)
	}

	@Test("allowed taxonomies")
	func allowedTaxonomies()
	{
		#expect(DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: 2022) == [.v6_5, .v6_6])
		#expect(DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: 2023) == [.v6_6, .v6_7])
		#expect(DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: 2024) == [.v6_7, .v6_8])
		#expect(DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: 2017) == [])
		#expect(DEGAAPTaxonomyVersion.allowedTaxonomies(businessYear: 2030) == [])
	}

	@Test("version comparison")
	func versionComparison()
	{
		#expect(DEGAAPTaxonomyVersion.v6_4 < .v6_5)
		#expect(DEGAAPTaxonomyVersion.v6_6 > .v6_5)
	}

	@Test("version description")
	func versionDescription()
	{
		#expect(DEGAAPTaxonomyVersion.v6_5.description == "6_5")
	}

	@Test("release date markers")
	func releaseDateMarkers()
	{
		#expect(DEGAAPTaxonomyVersion.v6_4.releaseDateMarker == "2020-04-01")
	}

	@Test("traits")
	func traits()
	{
		#expect(DEGAAPTaxonomyVersion.v6_3.traits.isEmpty)
		#expect(DEGAAPTaxonomyVersion.v6_4.traits == [.bvv])
		#expect(DEGAAPTaxonomyVersion.v6_6.traits == [.bvv])
	}
}
