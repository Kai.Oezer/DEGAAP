//  Copyright 2025 Kai Oezer

import Foundation
@testable import DEGAAP

class TestCodingArchive
{
	var data : String

	init(data : String = "")
	{
		self.data = data
	}
}

struct TestEncoder : Encoder
{
	var archive : TestCodingArchive
	var codingPath : [any CodingKey] = []
	var userInfo : [CodingUserInfoKey : Any] = [:]
	
	func container<Key : CodingKey>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key>
	{
		KeyedEncodingContainer(TestKeyedEncodingContainer(archive: archive))
	}

	func unkeyedContainer() -> UnkeyedEncodingContainer
	{
		TestUnkeyedEncodingContainer(archive: archive)
	}

	func singleValueContainer() -> any SingleValueEncodingContainer
	{
		TestSingleValueEncodingContainer(archive: archive)
	}
}

struct TestDecoder : Decoder
{
	var archive = TestCodingArchive()
	var codingPath : [any CodingKey] = []
	var userInfo : [CodingUserInfoKey : Any] = [:]

	func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey
	{
		KeyedDecodingContainer(TestKeyedDecodingContainer(archive: archive))
	}

	func unkeyedContainer() throws -> any UnkeyedDecodingContainer
	{
		TestUnkeyedDecodingContainer(archive: archive)
	}

	func singleValueContainer() throws -> any SingleValueDecodingContainer
	{
		TestSingleValueDecodingContainer(archive: archive)
	}
}

struct TestSingleValueDecodingContainer : SingleValueDecodingContainer
{
	var archive : TestCodingArchive
	var codingPath : [any CodingKey] = []
	func decodeNil() -> Bool { false }

	func decode(_ type: String.Type) throws -> String { archive.data }

	func decode<T>(_ type: T.Type) throws -> T where T : Decodable { fatalError("not implemented") }
	func decode(_ type: Bool.Type) throws -> Bool { false }
	func decode(_ type: Double.Type) throws -> Double { 0.0 }
	func decode(_ type: Float.Type) throws -> Float { 0.0 }
	func decode(_ type: Int.Type) throws -> Int { 0 }
	func decode(_ type: Int8.Type) throws -> Int8 { 0 }
	func decode(_ type: Int16.Type) throws -> Int16 { 0 }
	func decode(_ type: Int32.Type) throws -> Int32 { 0 }
	func decode(_ type: Int64.Type) throws -> Int64 { 0 }
	func decode(_ type: UInt.Type) throws -> UInt { 0 }
	func decode(_ type: UInt8.Type) throws -> UInt8 { 0 }
	func decode(_ type: UInt16.Type) throws -> UInt16 { 0 }
	func decode(_ type: UInt32.Type) throws -> UInt32 { 0 }
	func decode(_ type: UInt64.Type) throws -> UInt64 { 0 }
}

struct TestSingleValueEncodingContainer : SingleValueEncodingContainer
{
	var archive : TestCodingArchive
	var codingPath : [any CodingKey] = []

	mutating func encode(_ value: String) throws { archive.data = value }

	mutating func encode<T>(_ value: T) throws where T : Encodable {}
	mutating func encode(_ value: Int) throws {}
	mutating func encode(_ value: Int8) throws {}
	mutating func encode(_ value: Int16) throws {}
	mutating func encode(_ value: Int32) throws {}
	mutating func encode(_ value: Int64) throws {}
	mutating func encode(_ value: UInt) throws {}
	mutating func encode(_ value: UInt8) throws {}
	mutating func encode(_ value: UInt16) throws {}
	mutating func encode(_ value: UInt32) throws {}
	mutating func encode(_ value: UInt64) throws {}
	mutating func encode(_ value: Double) throws {}
	mutating func encode(_ value: Float) throws {}
	mutating func encode(_ value: Bool) throws {}
	mutating func encodeNil() throws {}
}

struct TestKeyedEncodingContainer<Key : CodingKey> : KeyedEncodingContainerProtocol
{
	var archive : TestCodingArchive
	var codingPath : [any CodingKey] = []

	mutating func nestedContainer<NestedKey : CodingKey>(keyedBy keyType: NestedKey.Type, forKey key: Key) -> KeyedEncodingContainer<NestedKey>
	{
		KeyedEncodingContainer(TestKeyedEncodingContainer<NestedKey>(archive: archive))
	}

	mutating func nestedUnkeyedContainer(forKey key: Key) -> any UnkeyedEncodingContainer
	{
		TestUnkeyedEncodingContainer(archive: archive)
	}

	mutating func superEncoder() -> any Encoder { TestEncoder(archive: archive) }
	mutating func superEncoder(forKey key: Key) -> any Encoder { TestEncoder(archive: archive) }

	mutating func encode(_ value: String, forKey key: Key) throws
	{
		archive.data = value
	}

	mutating func encode<T>(_ value: T, forKey key: Key) throws where T : Encodable {}
	mutating func encodeNil(forKey key: Key) throws {}
	mutating func encode(_ value: Int, forKey key: Key) throws {}
	mutating func encode(_ value: Int8, forKey key: Key) throws {}
	mutating func encode(_ value: Int16, forKey key: Key) throws {}
	mutating func encode(_ value: Int32, forKey key: Key) throws {}
	mutating func encode(_ value: Int64, forKey key: Key) throws {}
	mutating func encode(_ value: UInt, forKey key: Key) throws {}
	mutating func encode(_ value: UInt8, forKey key: Key) throws {}
	mutating func encode(_ value: UInt16, forKey key: Key) throws {}
	mutating func encode(_ value: UInt32, forKey key: Key) throws {}
	mutating func encode(_ value: UInt64, forKey key: Key) throws {}
	mutating func encode(_ value: Double, forKey key: Key) throws {}
	mutating func encode(_ value: Float, forKey key: Key) throws {}
	mutating func encode(_ value: Bool, forKey key: Key) throws {}
}

struct TestKeyedDecodingContainer<Key : CodingKey> : KeyedDecodingContainerProtocol
{
	var archive : TestCodingArchive

	var codingPath: [any CodingKey] = []

	var allKeys : [Key] = []

	func contains(_ key: Key) -> Bool { true }

	func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey
	{
		KeyedDecodingContainer(TestKeyedDecodingContainer<NestedKey>(archive: archive))
	}

	func nestedUnkeyedContainer(forKey key: Key) throws -> any UnkeyedDecodingContainer
	{
		TestUnkeyedDecodingContainer(archive: archive)
	}
	
	func superDecoder() throws -> any Decoder { TestDecoder(archive: archive) }
	func superDecoder(forKey key: Key) throws -> any Decoder { TestDecoder(archive: archive) }

	func decode(_ type: String.Type, forKey key: Key) throws -> String
	{
		archive.data
	}

	func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T : Decodable { fatalError("not implemented") }
	func decode(_ type: Int.Type, forKey key: Key) throws -> Int { 0 }
	func decode(_ type: Int8.Type, forKey key: Key) throws -> Int8 { 0 }
	func decode(_ type: Int16.Type, forKey key: Key) throws -> Int16 { 0 }
	func decode(_ type: Int32.Type, forKey key: Key) throws -> Int32 { 0 }
	func decode(_ type: Int64.Type, forKey key: Key) throws -> Int64 { 0 }
	func decode(_ type: UInt.Type, forKey key: Key) throws -> UInt { 0 }
	func decode(_ type: UInt8.Type, forKey key: Key) throws -> UInt8 { 0 }
	func decode(_ type: UInt16.Type, forKey key: Key) throws -> UInt16 { 0 }
	func decode(_ type: UInt32.Type, forKey key: Key) throws -> UInt32 { 0 }
	func decode(_ type: UInt64.Type, forKey key: Key) throws -> UInt64 { 0 }
	func decode(_ type: Double.Type, forKey key: Key) throws -> Double { 0.0 }
	func decode(_ type: Float.Type, forKey key: Key) throws -> Float { 0.0 }
	func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool { false }
	func decodeNil(forKey key: Key) throws -> Bool { false }
}

struct TestUnkeyedEncodingContainer : UnkeyedEncodingContainer
{
	var archive : TestCodingArchive
	var codingPath: [any CodingKey] = []

	var count : Int = 0

	mutating func nestedContainer<NestedKey : CodingKey>(keyedBy keyType: NestedKey.Type) -> KeyedEncodingContainer<NestedKey>
	{
		KeyedEncodingContainer(TestKeyedEncodingContainer(archive: archive))
	}
	
	mutating func nestedUnkeyedContainer() -> any UnkeyedEncodingContainer
	{
		TestUnkeyedEncodingContainer(archive: archive)
	}
	
	mutating func superEncoder() -> any Encoder
	{
		TestEncoder(archive: archive)
	}

	mutating func encode(_ value: String) throws
	{
		archive.data = value
	}

	mutating func encode<T>(_ value: T) throws where T : Encodable {}
	mutating func encode(_ value: Int) throws {}
	mutating func encode(_ value: Int8) throws {}
	mutating func encode(_ value: Int16) throws {}
	mutating func encode(_ value: Int32) throws {}
	mutating func encode(_ value: Int64) throws {}
	mutating func encode(_ value: UInt) throws {}
	mutating func encode(_ value: UInt8) throws {}
	mutating func encode(_ value: UInt16) throws {}
	mutating func encode(_ value: UInt32) throws {}
	mutating func encode(_ value: UInt64) throws {}
	mutating func encode(_ value: Double) throws {}
	mutating func encode(_ value: Float) throws {}
	mutating func encode(_ value: Bool) throws {}
	mutating func encodeNil() throws {}
}

struct TestUnkeyedDecodingContainer : UnkeyedDecodingContainer
{
	var archive : TestCodingArchive
	var codingPath : [any CodingKey] = []

	var count : Int?

	var isAtEnd : Bool = true

	var currentIndex : Int = 0

	mutating func nestedContainer<NestedKey : CodingKey>(keyedBy type: NestedKey.Type) throws -> KeyedDecodingContainer<NestedKey>
	{
		KeyedDecodingContainer(TestKeyedDecodingContainer<NestedKey>(archive: archive))
	}
	
	mutating func nestedUnkeyedContainer() throws -> any UnkeyedDecodingContainer
	{
		TestUnkeyedDecodingContainer(archive: archive)
	}
	
	mutating func superDecoder() throws -> any Decoder
	{
		TestDecoder()
	}

	mutating func decode(_ type: String.Type) throws -> String
	{
		archive.data
	}

	mutating func decode<T>(_ type: T.Type) throws -> T where T : Decodable { fatalError("not implemented") }
	mutating func decode(_ type: Int.Type) throws -> Int { 0 }
	mutating func decode(_ type: Int8.Type) throws -> Int8 { 0 }
	mutating func decode(_ type: Int16.Type) throws -> Int16 { 0 }
	mutating func decode(_ type: Int32.Type) throws -> Int32 { 0 }
	mutating func decode(_ type: Int64.Type) throws -> Int64 { 0 }
	mutating func decode(_ type: UInt.Type) throws -> UInt { 0 }
	mutating func decode(_ type: UInt8.Type) throws -> UInt8 { 0 }
	mutating func decode(_ type: UInt16.Type) throws -> UInt16 { 0 }
	mutating func decode(_ type: UInt32.Type) throws -> UInt32 { 0 }
	mutating func decode(_ type: UInt64.Type) throws -> UInt64 { 0 }
	mutating func decode(_ type: Double.Type) throws -> Double { 0.0 }
	mutating func decode(_ type: Float.Type) throws -> Float { 0.0 }
	mutating func decode(_ type: Bool.Type) throws -> Bool { false }
	mutating func decodeNil() throws -> Bool { false }
}


