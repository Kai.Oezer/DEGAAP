//  Copyright 2025 Kai Oezer

import Testing
import Foundation
@testable import DEGAAP

@Suite("Miscellaneous tests")
struct MiscellaneousTests
{
	@Test("DEGAAPChartDescriptor to string")
	func chartDescriptorToString()
	{
		#expect(DEGAAPChartDescriptor(taxonomy: .v6_7).description == "6_7-fiscal")
		#expect(DEGAAPChartDescriptor(taxonomy: .v6_8, fiscal: false).description == "6_8-trade")
	}

	@Test("ChartItem to string", .tags(.itemDescription))
	func chartItemDescription()
	{
		let chartItem = ChartItem(
			id: DEGAAPItemID("bla.bla.hello")!,
			section: .notes,
			balanceType: .none,
			definition: "example chart item",
			definitionGerman: "Beispielkonto"
		)
		#expect(chartItem.description == "bla.bla.hello : example chart item")
	}

	@Test("DEGAAPSourceChartItemID initialization", .tags(.sourceChartItem))
	func sourceChartItemIDInitialization()
	{
		#expect(DEGAAPSourceChartItemID("9999") != nil)
		#expect(DEGAAPSourceChartItemID("19999") == nil)
		#expect(DEGAAPSourceChartItemID("-9999") == nil)
		#expect(DEGAAPSourceChartItemID("abcd") == nil)
	}

	@Test("coding", .tags(.sourceChartItem))
	func sourceChartItemIDDecoding() async throws
	{
		let sourceChartName = "1234"
		let archive = TestCodingArchive(data: sourceChartName)
		let decoder = TestDecoder(archive: archive)
		let decodedItemID = try DEGAAPSourceChartItemID(from: decoder)
		#expect (decodedItemID == DEGAAPSourceChartItemID(sourceChartName)!)

		#expect(throws: DecodingError.self) {
			let invalidArchive = TestCodingArchive(data: "123456")
			_ = try DEGAAPSourceChartItemID(from: TestDecoder(archive: invalidArchive))
		}
	}

	@Test("DEGAAPSourceChartGroup identifier", .tags(.sourceChartGroup))
	func sourceChartGroupIdentifier()
	{
		let item1a = DEGAAPSourceChartItemID("1000")!
		let item1b = DEGAAPSourceChartItemID("1999")!
		let group1 = DEGAAPSourceChartGroup(range: item1a...item1b, description: "1")
		#expect(group1.id == item1a)
	}

	@Test("DEGAAPSourceChartGroup comparison", .tags(.sourceChartGroup))
	func sourceChartGroupComparison()
	{
		let item1a = DEGAAPSourceChartItemID("1000")!
		let item1b = DEGAAPSourceChartItemID("1999")!
		let item2a = DEGAAPSourceChartItemID("2000")!
		let item2b = DEGAAPSourceChartItemID("2999")!
		let group1 = DEGAAPSourceChartGroup(range: item1a...item1b, description: "1")
		let group2 = DEGAAPSourceChartGroup(range: item2a...item2b, description: "2")
		let group4 = DEGAAPSourceChartGroup(range: item1a...item1b, description: "4")
		#expect(group1 != group2)
		#expect(group1 == group4)
		#expect(group1 < group2)
		#expect(group2 > group1)
	}

	@Test("DEGAAPSourceChartCategory comparison", .tags(.sourceChartGroup))
	func sourceChartCategoryComparison()
	{
		let category1 = DEGAAPSourceChartCategory(id: 1, description: "1")
		let category2 = DEGAAPSourceChartCategory(id: 2, description: "2")
		let category3 = DEGAAPSourceChartCategory(id: 1, description: "3")
		#expect(category1 != category2)
		#expect(category1 == category3)
		#expect(category1 < category2)
	}

}
