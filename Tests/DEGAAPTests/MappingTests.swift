//  Copyright 2020-2025 Kai Oezer

import Foundation
import Testing
import IssueCollection
@testable import DEGAAP

@Suite("Mapping tests", .tags(.chartMapping))
struct MappingTests
{
	let _miniTestMapping : Mapping?

	init()
	{
		if let mappingFileLocation = Bundle.module.url(forResource: "mini-mapping", withExtension: "csv", subdirectory: "Resources") {
			_miniTestMapping = try? Mapping(descriptor: .init(source: .skr03, target: .init(taxonomy: .v6_7, fiscal: true)), mappingTableLocation: mappingFileLocation)
		} else {
			_miniTestMapping = nil
		}
	}

	@Test("Loading and verification")
	func loading() async throws
	{
		let mapping = try #require(_miniTestMapping)
		#expect(mapping.chartMap.count == 48)

		let chartFileLocation = try #require (Bundle.module.url(forResource: "mini-gaap-tax56", withExtension: "csv", subdirectory: "Resources"))
		let chart = try Chart(fileLocation: chartFileLocation)
		var issues = IssueCollection()
		DEGAAPEnvironment.verifyMappedItems(in: mapping, using: chart, issues: &issues)
		#expect(issues.count == 4)
		let issuesWithItemsMissingFromChart = issues.filter{ $0.code == .mappedItemMissingFromChart }
		#expect(issuesWithItemsMissingFromChart.count == 3)
		#expect(issuesWithItemsMissingFromChart.filter{ issue in issue.metadata[.mappedItem] == "is.netIncome.tax.kest"}.count == 1)
		#expect(issuesWithItemsMissingFromChart.filter{ issue in issue.metadata[.mappedItem] == "bs.ass.fixAss.tan.inConstrAdvPaym"}.count == 2)
		let issuesWithItemBeingSummationItem = issues.filter{ $0.code == .mappedItemIsSummationItem }
		try #require(issuesWithItemBeingSummationItem.count == 1)
		#expect(issuesWithItemBeingSummationItem.first!.metadata[.mappedItem] == "bs.ass.fixAss.tan.otherEquipm")
	}

	@Test("Loading nonexistent mapping table")
	func loadingNonexistentMappingTable() async throws
	{
		let resourcesRootLocation = try #require(Bundle.module.resourceURL)
		let mappingFileLocation = resourcesRootLocation.appending(path: "bogusFile.csv", directoryHint: .notDirectory)
		#expect(throws: MappingError.self) {
			let _ = try Mapping(descriptor: .init(source: .skr03, target: .init(taxonomy: .v6_7, fiscal: true)), mappingTableLocation: mappingFileLocation)
		}
		#expect(throws: MappingError.self) {
			let _ = try Mapping(descriptor: .init(source: .skr04, target: .init(taxonomy: .v6_8)))
		}
	}

#if false
	func mappingForTesting() async throws
	{
		measure(metrics: [XCTClockMetric()]) {
			let exp = expectation(description: "finished searching mapped items")
			Task {
				let mapping = try #require(await environment.mappingForTesting)
				let itemResult1 = await environment.chartItem(for: "1666", mapping: mapping)
				#expect(itemResult1 != nil)
				let itemResult2 = await environment.chartItem(for: "4855", mapping: mapping)
				#expect(itemResult2 != nil)
				exp.fulfill()
			}
			wait(for: [exp])
		}
	}
#endif

	@Test("Source items with description", .tags(.itemDescription))
	func sourceItemsWithDescription() async throws
	{
		let mapping = try #require(_miniTestMapping)
		let itemsWithDescription = mapping.itemsWithDescription
		#expect(itemsWithDescription.count == 48)
		let thresholdID = DEGAAPSourceChartItemID("0500")!
		let sortedShortenedItemsWithDescriptions = itemsWithDescription.filter{ $0.0 < thresholdID }.sorted{ $0.0 < $1.0 }
		let itemsAndDescriptionsString = sortedShortenedItemsWithDescriptions.map{ "\($0.0) \($0.1)" }.joined(separator: "\n")
		#expect(itemsAndDescriptionsString == """
			0010 Entgeltlich erworbene Konzessionen, gewerbliche Schutzrechte und ähnliche Rechte und Werte sowie Lizenzen an solchen Rechten und Werten
			0150 Wohnbauten im Bau auf eigenen Grundstücken
			0159 Anzahlungen auf Wohnbauten auf eigenen Grundstücken
			0160 Bauten auf fremden Grundstücken
			0165 Geschäftsbauten (fremde Grundstücke)
			0300 Andere Anlagen, Betriebs- und Geschäftsausstattung
			0320 Pkw
			0350 Lkw
			""")
	}

	@Test("description for source chart item")
	func descriptionForSourceChartItem() async throws
	{
		let mapping = try #require(_miniTestMapping)
		let description = mapping.description(for: DEGAAPSourceChartItemID("1546")!)
		#expect(description == "Umsatzsteuerforderungen Vorjahr")
	}
}
