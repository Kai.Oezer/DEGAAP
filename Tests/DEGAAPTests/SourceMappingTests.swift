//  Copyright 2024-2025 Kai Oezer

import Testing
import Foundation
@testable import DEGAAP

@Suite("SourceMapping tests")
struct SourceMappingTests
{
	let _miniTestMapping : SourceMapping?

	init()
	{
		if let mappingFileLocation = Bundle.module.url(forResource: "mini-source-mapping", withExtension: "csv", subdirectory: "Resources") {
			_miniTestMapping = SourceMapping(type: .ikr, mappingTableLocation: mappingFileLocation)
		} else {
			_miniTestMapping = nil
		}
	}

	@Test("DEGAAPSourceChartItemID tests")
	func itemID() throws
	{
		#expect(DEGAAPSourceChartItemID("") == nil)
		#expect(DEGAAPSourceChartItemID("invalid<%$>ID") == nil)
		#expect(DEGAAPSourceChartItemID("123") == nil)
		#expect(DEGAAPSourceChartItemID("-1234") == nil)
		#expect(DEGAAPSourceChartItemID("+1234") == nil)
		#expect(DEGAAPSourceChartItemID("1234")?.code == "1234")
		#expect(DEGAAPSourceChartItemID.none == .init("0000")!)
		#expect(DEGAAPSourceChartItemID.none.description.isEmpty)
	}

	@Test("Loading the mini test mapping table")
	func loading() throws
	{
		let mapping = try #require(_miniTestMapping)
		#expect(mapping[DEGAAPSourceChartItemID("4769")!] == DEGAAPSourceChartItemID("8769")!)
		#expect(mapping[DEGAAPSourceChartItemID("1600")!] == DEGAAPSourceChartItemID("1000")!)
		#expect(mapping.description(for: DEGAAPSourceChartItemID("8500")!) == nil)
		#expect(mapping.description(for: DEGAAPSourceChartItemID("1800")!) == "Bank")
		#expect(mapping.description(for: DEGAAPSourceChartItemID("4837")!) == "Sonstige Erträge betriebsfremd und regelmäßig")
	}

	@Test("Loading nonexistent mapping")
	func loadingNonExistentMappingTable() async throws
	{
		let resourceURL = try #require(Bundle.module.resourceURL)
		let mappingFileLocation = resourceURL.appendingPathComponent("bogus-mapping.csv")
		#expect(SourceMapping(type: .ikr, mappingTableLocation: mappingFileLocation) == nil)
	}

	@Test("Finding source chart items by description", arguments: [
		("Sonstige betriebliche Erträge", true, ["4830", "4832", "4842"]),
		("Sonstige BETRIEBLICHE Erträge", true, []),
		("Sonstige BETRIEBLICHE Erträge", false, ["4830", "4832", "4842"]),
		("Sonstige betriebliche Erträge von verbundenen Unternehmen", false, ["4832"])
	])
	func findingChartItemsByDescription(searchString : String, caseSensitive : Bool, expectedItems : [String]) async throws
	{
		let mapping = try #require(_miniTestMapping)
		let foundItems = mapping.items(describedBy: searchString, caseSensitive: caseSensitive)
		let foundItemNames = foundItems.map{ $0.0.description }.sorted()
		#expect(foundItemNames == expectedItems)
	}

	@Test("Source items with descriptions", .tags(.itemDescription))
	func sourceItemsWithDescriptions() throws
	{
		let mappingFileLocation = try #require(Bundle.module.url(forResource: "mini-source-mapping", withExtension: "csv", subdirectory: "Resources"))
		let mapping = try #require(SourceMapping(type: .ikr, mappingTableLocation: mappingFileLocation))
		let itemsAndDescriptions = mapping.itemsWithDescription
		#expect(itemsAndDescriptions.count == 53)
		let thresholdID = DEGAAPSourceChartItemID("2000")!
		let sortedShortListOfItemsAndDescriptions = itemsAndDescriptions.filter{ $0.0 < thresholdID }.sorted{ $0.0 < $1.0 }
		let itemsAndDescriptionsString = sortedShortListOfItemsAndDescriptions.map{ "\($0.0) \($0.1)" }.joined(separator: "\n")
		#expect(itemsAndDescriptionsString == """
			1550 Schecks
			1600 Kasse
			1610 Nebenkasse 1
			1620 Nebenkasse 2
			1700 Bank (Postbank)
			1710 Bank (Postbank 1)
			1720 Bank (Postbank 2)
			1730 Bank (Postbank 3)
			1780 LZB-Guthaben
			1790 Bundesbankguthaben
			1800 Bank
			1810 Bank 1
			1820 Bank 2
			""")
	}

}
