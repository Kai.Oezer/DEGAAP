// Copyright 2023-2024 Kai Oezer

import DEGAAP
import ArgumentParser
import Dispatch

extension DEGAAPParsableCommand
{
	struct SearchCommand : ParsableCommand
	{
		static let configuration = CommandConfiguration(abstract: "Searches the DE-GAAP tables.")

		@OptionGroup var options : DEGAAPParsableCommand.Options

		mutating func run() async throws
		{
			let environment = DEGAAPEnvironment()
			let chart = DEGAAPChartDescriptor(taxonomy: options.taxonomy, fiscal: options.fiscal)
			guard let path = await environment.itemPath(for: options.account, in: chart) else {
				print("Could not load \(options.fiscal ? "fiscal " : "")DE-GAAP chart for taxonomy \(options.taxonomy)")
				return
			}
			guard !path.isEmpty else {
				print("Item not found.")
				return
			}
			print("Path:\n\(path.map{ "  " + $0.description }.joined(separator: "\n"))")
		}
	}
}
