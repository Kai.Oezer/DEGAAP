// Copyright 2023 Kai Oezer

import Foundation

#if canImport(OSLog)

import OSLog

let logger = os.Logger(subsystem: "DEGAAP", category: "DEGAAP")

#else

import Logging

let logger = Logging.Logger(label: "DEGAAP")

#endif
