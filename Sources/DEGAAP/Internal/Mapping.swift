// Copyright 2020-2025 Kai Oezer

import Foundation
import CodableCSV

enum MappingError : Swift.Error
{
	case mappingNotSupported
	case mappingTableNotFound
	case mappingTableLoading(String)
}

extension MappingError : CustomDebugStringConvertible
{
	var debugDescription: String
	{
		switch self {
			case let .mappingTableLoading(errorDescription): return errorDescription
			case .mappingTableNotFound: return "The mapping table file could not be found."
			case .mappingNotSupported: return "The selected mapping is not supported."
		}
	}
}

// MARK: -

/// Abstraction for a mapping to a DE-GAAP chart from some accounts chart.
class Mapping
{
	let descriptor : DEGAAPMappingDescriptor

	var chartMap = [DEGAAPSourceChartItemID : DEGAAPItemID]()
	var descriptionMap = [DEGAAPSourceChartItemID : String]()
	var inverseDescriptionMap = [String : DEGAAPSourceChartItemID]()

	convenience init(descriptor : DEGAAPMappingDescriptor) throws(MappingError)
	{
		let filename = "Mapping-\(descriptor.source)-to-DEGAAP-\(descriptor.target.taxonomy)"
		guard let tableLocation = Bundle.module.url(forResource: filename, withExtension: "csv", subdirectory: "Resources") else {
			throw MappingError.mappingTableNotFound
		}
		try self.init(descriptor: descriptor, mappingTableLocation: tableLocation)
	}

	init(descriptor : DEGAAPMappingDescriptor, mappingTableLocation : URL) throws(MappingError)
	{
		self.descriptor = descriptor
		do {
			let mappingData = try _load(from: mappingTableLocation)
			// swiftlint:disable large_tuple
			(chartMap, descriptionMap, inverseDescriptionMap) = mappingData
		} catch {
			throw MappingError.mappingTableLoading("Could not load chart mapping from \(descriptor.source) to \(descriptor.target). \(error)")
		}
	}

	subscript(itemID : DEGAAPSourceChartItemID) -> DEGAAPItemID?
	{
		chartMap[itemID]
	}

	/// - Returns : The description string associated with the mapping for the given source chart item ID.
	func description(for itemID : DEGAAPSourceChartItemID) -> String?
	{
		descriptionMap[itemID]
	}

	typealias DescriptionMapPair = (DEGAAPSourceChartItemID, String)

	/// - Returns: The source chart items paired with their descriptions
	var itemsWithDescription : [(DEGAAPSourceChartItemID, String)]
	{
		descriptionMap.compactMap{ $0 }
	}

	/// - returns: A list of item ID and description pairs, in which the given `searchKey` occurs in either part.
	func items(describedBy searchKey : String, caseSensitive : Bool = false) -> [DescriptionMapPair]
	{
		let locale = Locale(identifier: "de_DE")
		let keyDoesOccur : (String) -> Bool = { text in
			caseSensitive ? text.contains(searchKey) : (text.range(of: searchKey, options: .caseInsensitive, locale: locale) != nil)
		}
		return descriptionMap.filter {
			keyDoesOccur($0.key.code) || keyDoesOccur($0.value)
		}
	}

	private typealias LoadedMappingData = ([DEGAAPSourceChartItemID : DEGAAPItemID], [DEGAAPSourceChartItemID : String], [String : DEGAAPSourceChartItemID])

	private func _load(from tableLocation : URL) throws -> LoadedMappingData
	{
		let decoder = CSVDecoder {
			$0.delimiters.field = ";"
			$0.delimiters.row = "\n"
			$0.headerStrategy = .firstLine
			$0.encoding = .utf8
			$0.nilStrategy = .empty
			$0.presample = false
			$0.decimalStrategy = .locale(Locale(languageCode: .german, script: .latin, languageRegion: .germany))
			$0.dataStrategy = .base64
			$0.bufferingStrategy = .sequential
			$0.trimStrategy = .whitespaces
			$0.escapingStrategy = .doubleQuote
		}
		let mapItems = try decoder.decode(Array< MappingItem<String> >.self, from: tableLocation)
		var mapping = [DEGAAPSourceChartItemID : DEGAAPItemID]()
		var descriptionMapping = [DEGAAPSourceChartItemID : String]()
		var inverseDescriptionMapping = [String : DEGAAPSourceChartItemID]()
		mapItems.forEach {
			if let itemID = $0.itemID,
				 let degaapID = $0.degaapID {
				mapping[itemID] = degaapID
				if let description = $0.description, !description.isEmpty
				{
					descriptionMapping[$0.itemID!] = description
					inverseDescriptionMapping[description] = $0.itemID!
				}
			}
		}
		return (mapping, descriptionMapping, inverseDescriptionMapping)
	}

	private struct MappingItem<T> : Decodable where T : Decodable
	{
		let itemID : DEGAAPSourceChartItemID?
		let degaapID : DEGAAPItemID?
		let description : String?

		// swiftlint:disable nesting
		enum CodingKeys : String, CodingKey
		{
			case itemID = "ID"
			case degaapID = "de-gaap-ci"
			case description = "description"
		}
	}

}

extension Mapping : CustomStringConvertible
{
	public var description : String
	{
		"\(descriptor.source.rawValue) to taxonomy \(descriptor.target)"
	}
}
