// Copyright 2020-2024 Kai Oezer

import Foundation
import RegexBuilder

/// This is an adapter for use with CSVDecoder to extract DEGAAPItem data
/// from entries in the CSV-formatted DE-GAAP-CI MicroBilG items file.
struct TaxonomyItem : Decodable
{
	let abstract : TIBool?
	let balanceType : ChartItem.BalanceType
	let periodType : TIPeriodType
	let valueType : TIValueType?
	let id : DEGAAPItemID?
	let reportSection : TIReportSection?
	let calcWeight : TIPlusMinus?
	let calcParent : TIParentItem? // if there is a calculatory link (as indicated by `requirement`) to a parent item
	let level : Int?
	let definitionDE : String?
	let definitionEN : String?
	let shortDefinitionDE : String?
	let shortDefinitionEN : String?
	let documentation : String?
	let usage : String?
	let lawShortName : String?
	let lawParagraph : String?
	let lawSubparagraph : String?
	let notFor : TIUsageRestriction?
	let requirement : ChartItem.ReportingRequirement?
	let resultType : TIResultType?
	let legalFormEU : TIBool? // item is valid only for Einzelunternehmen
	let legalFormPG : TIBool? // item is valid only for Einzelunternehmen
	let legalFormKSt : TIBool? // item is valid only for Einzelunternehmen

	enum CodingKeys : String, CodingKey
	{
		case abstract = "abstract"
		case balanceType = "balance"
		case periodType = "periodType"
		case valueType = "type"
		case id = "name"
		case reportSection = "Berichtsteil"
		case calcWeight = "+/-"
		case calcParent = "Rechnerisch verknüpft mit"
		case level = "lvl"
		case definitionDE = "standard de"
		case definitionEN = "standard en"
		case shortDefinitionDE = "terse de"
		case shortDefinitionEN = "terse en"
		case documentation = "documentation de"
		case usage = "definitionGuidance de"
		case lawShortName = "law name"
		case lawParagraph = "law paragraph"
		case lawSubparagraph = "law subparagraph"
		case notFor = "notPermittedFor"
		case requirement = "fiscalRequirement"
		case resultType = "typeOperatingResult"
		case legalFormEU = "legalFormEU"
		case legalFormPG = "legalFormPG"
		case legalFormKSt = "legalFormKSt"
	}

	enum TIBool : String, Decodable
	{
		case tiTrue = "true"
		case tiFalse = "false"
	}

	enum TIValueType : String, Decodable
	{
		case money = "xbrli:monetaryItemType"
		case string = "xbrli:stringItemType"
		case uri = "xbrli:anyURIItemType"
		case date = "xbrli:dateItemType"
		case long = "xbrli:longItemType"
		case decimal = "xbrli:decimalItemType"
		case qName = "xbrli:QNameItemType"
		case pure = "xbrli:pureItemType"
		case boolean = "xbrli:booleanItemType"
	}

	enum TIPeriodType : String, Decodable
	{
		case none = ""
		case instant = "instant"
		case duration = "duration"
	}

	enum TIUsageRestriction : String, Decodable
	{
		case notForTradeReport = "handelsrechtlich"
		case notForTaxReport = "steuerlich"
		case notForElectronicTax = "Einreichung an Finanzverwaltung"
		case notForElectronicTaxOrTrade = "Einreichung an Finanzverwaltung ; handelsrechtlich"
	}

	enum TIReportSection : String, Decodable
	{
		case balanceSheet          = "Bilanz"
		case noteOnBalanceSheet    = "Angaben unter der Bilanz"
		case incomeStatement       = "Gewinn- und Verlustrechnung nach MicroBilG"
		case incomeUse             = "Ergebnisverwendung"
		case kke                   = "Kapitalkontenentwicklung für Personenhandelsgesellschaften"
		case equityChangeStatement = "Eigenkapitalspiegel"
		case cashFlowStatement1    = "Kapitalflussrechnung DRS 2"
		case cashFlowStatement2    = "Kapitalflussrechnung DRS 21"
		case appendix              = "Anhang"
		case statusReport          = "Lagebericht"
		case valueChange           = "Steuerliche Modifikationen"
		case creditworthiness      = "Zusatzinformation Kreditwürdigkeitsprüfung"
		case otherSection          = "Andere Berichtsbestandteile"
		case detailInformation     = "Detailinformationen zu Positionen"
		case tpl                   = "Berichtigung des Gewinns bei Wechsel der Gewinnermittlungsart"
		case fpl                   = "Steuerliche Gewinnermittlung"
		case fplgm                 = "Steuerliche Gewinnermittlung bei Feststellungsverfahren"
		case specialCase           = "Steuerliche Gewinnermittlung für besondere Fälle"
		case bvv                   = "Steuerlicher Betriebsvermögensvergleich"
	}

	enum TIPlusMinus : String, Decodable
	{
		case plus = "+"
		case minus = "-"
	}

	struct TIParentItem : Decodable
	{
		let itemName : String

		init(_ name : String)
		{
			itemName = name
		}

		init(from decoder: Decoder) throws
		{
			let content = try decoder.singleValueContainer().decode(String.self)
		#if true
			let pattern = Regex {
				Anchor.startOfLine
				"Oberposition ("
				Capture( OneOrMore(.any, .reluctant) )
				")"
				Anchor.endOfLine
			}
		#else
			let pattern = /^Oberposition\p{Z}{1}\((.+)\)$/
		#endif
			guard let (_, name) = content.wholeMatch(of: pattern)?.output else {
				throw DecodingError.valueNotFound(Self.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Oberposition missing"))
			}
			itemName = String(name)
		}
	}

	enum TIResultType : String, Decodable
	{
		case gkv = "GKV" /// Gesamtkostenverfahren
		case ukv = "UKV" /// Umsatzkostenverfahren
		case both = "neutral"
	}
}
