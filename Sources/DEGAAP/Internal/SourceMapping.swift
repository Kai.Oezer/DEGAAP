//  Copyright 2024-2025 Kai Oezer

import Foundation
import CodableCSV

/// Encapsulates data and functions for handling the mapping of accounts
/// of a given DEGAAPSourceChart (other than SKR 03) to SKR 03 accounts.
class SourceMapping
{
	typealias TargetItemAndSourceDescription = (DEGAAPSourceChartItemID, String?)

	let chartType : DEGAAPSourceChart
	private let _forwardMapping : [DEGAAPSourceChartItemID : TargetItemAndSourceDescription]
	private let _inverseMapping : [DEGAAPSourceChartItemID : Set<DEGAAPSourceChartItemID>]

	convenience init?(type : DEGAAPSourceChart)
	{
		let filename = "SourceMapping-\(type)-to-SKR03"
		guard let fileLocation = Bundle.module.url(forResource: filename, withExtension: "csv", subdirectory: "Resources") else { return nil }
		self.init(type: type, mappingTableLocation: fileLocation)
	}

	init?(type : DEGAAPSourceChart, mappingTableLocation : URL)
	{
		guard type != .skr03 else { return nil }
		self.chartType = type
		do {
			var forwardMapping = [DEGAAPSourceChartItemID : TargetItemAndSourceDescription]()
			var inverseMapping = [DEGAAPSourceChartItemID : Set<DEGAAPSourceChartItemID>]()
			let chartItems = try Self._load(from: mappingTableLocation)
			chartItems.forEach {
				forwardMapping[$0.key] = $0.value
				inverseMapping[$0.value.0]?.insert($0.key)
			}
			_forwardMapping = forwardMapping
			_inverseMapping = inverseMapping
		} catch {
			logger.error("Could not load source chart mapping from \(mappingTableLocation). \(error)")
			return nil
		}
	}

	subscript(itemID : DEGAAPSourceChartItemID) -> DEGAAPSourceChartItemID?
	{
		_forwardMapping[itemID]?.0
	}

	/// - Returns: The description of the source chart item with the given ID,
	/// using the nomenclature of the source chart.
	func description(for itemID : DEGAAPSourceChartItemID) -> String?
	{
		_forwardMapping[itemID]?.1
	}

	/// - Returns: A list of source chart item ID and description pairs,
	/// where either the item ID or the description contains the given search key
	/// - Parameters:
	///   - searchKey : The sub-string to be searched.
	///   - caseSensitive: Whether the match is required to be case sensitive.
	func items(describedBy searchKey : String, caseSensitive : Bool) -> [(DEGAAPSourceChartItemID, String?)]
	{
		_forwardMapping.filter{
			($0.key.code.range(of: searchKey, options: caseSensitive ? [] : .caseInsensitive) != nil)
			|| ($0.value.1?.range(of: searchKey, options: caseSensitive ? [] : .caseInsensitive) != nil)
		}.map{
			($0.0, $0.1.1)
		}
	}

	/// - Returns: The pairs of source items with description
	var itemsWithDescription : [(DEGAAPSourceChartItemID, String)]
	{
		_forwardMapping.compactMap{ $0.value.1 != nil ? ($0.key, $0.value.1!) : nil }
	}

	private static func _load(from tableLocation : URL) throws -> [DEGAAPSourceChartItemID : (DEGAAPSourceChartItemID, String?)]
	{
		let decoder = CSVDecoder {
			$0.delimiters.field = ";"
			$0.delimiters.row = "\n"
			$0.headerStrategy = .firstLine
			$0.encoding = .utf8
			$0.nilStrategy = .empty
			$0.presample = false
			$0.decimalStrategy = .locale(Locale(languageCode: .german, script: .latin, languageRegion: .germany))
			$0.dataStrategy = .base64
			$0.bufferingStrategy = .sequential
			$0.trimStrategy = .whitespaces
			$0.escapingStrategy = .doubleQuote
		}
		let chartItems = try decoder.decode(Array< SourceMappingItem<String> >.self, from: tableLocation)
		var chartItemMapping = [DEGAAPSourceChartItemID : (DEGAAPSourceChartItemID, String?)]()
		chartItems.forEach {
			if let source = $0.source, let target = $0.target {
				chartItemMapping[source] = (target, $0.description)
			}
		}
		return chartItemMapping
	}

	private struct SourceMappingItem<T> : Decodable where T : Decodable
	{
		let source : DEGAAPSourceChartItemID?
		let target : DEGAAPSourceChartItemID?
		let description : String? /// the description of the chart item, using source chart nomenclature
	}

}
