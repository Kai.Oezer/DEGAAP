//  Copyright 2020-2025 Kai Oezer

import Foundation
import RFTokenizableKeyTree
import CodableCSV

/**
	The chart of accounts for small companies in Germany,
	based on the HGB taxonometry of [XBRL Deutschland e.V.](https://de.xbrl.org/)
*/
class Chart
{
	typealias ChartItemTree = RFTokenizableKeyTree<ExpandableChartItem>
	private let _chartItemTree : ChartItemTree
	private let _incomeStatementInverseLinkbase : any Linkbase
	private let _balanceSheetInverseLinkbase : any Linkbase

	/// Loads a chart file based on the given taxonomy version and legal type.
	/// - Parameter taxonomy: The taxonomy version of the chart to be loaded.
	/// - Parameter fiscal: Whether to load a fiscal chart (`true`) or a trade chart (`false`).
	convenience init(taxonomy : DEGAAPTaxonomyVersion, fiscal : Bool) throws(DEGAAPError)
	{
		let filename = "de-gaap-ci-\(taxonomy.releaseDateMarker)-shell-\(fiscal ? "fiscal-" : "")microbilg"
		guard let tableLocation = Bundle.module.url(forResource: filename, withExtension: "csv", subdirectory: "Resources") else {
			throw DEGAAPError.taxonomyLoading("Could not locate DE-GAAP-CI chart definition file.")
		}
		try self.init(fileLocation: tableLocation)
	}
	
	init(fileLocation : URL) throws(DEGAAPError)
	{
		let taxonomyItems = try Self._loadTaxonomyItemsFromFile(at: fileLocation)
		let chartItems = Self._createChartItems(from: taxonomyItems)
		_chartItemTree = ChartItemTree(items: chartItems)
		
#if CREATE_LINKBASE_FROM_KERNTAXONOMIE
		_incomeStatementInverseLinkbase = TILinkbase(taxonomyItems: taxonomyItems, reportSection: .incomeStatement).reverse
		_balanceSheetInverseLinkbase = TILinkbase(taxonomyItems: taxonomyItems, reportSection: .balanceSheet).reverse
#else
		let (incomeStatementLinkbase, incomeStatementLinkbaseMicroBilG, balanceSheetLinkbase) = try XBRLLinkbase.linkbaseGroup(for: taxonomy)
		_incomeStatementInverseLinkbase = (incomeStatementLinkbase.joining(with: incomeStatementLinkbaseMicroBilG)).reverse
		_balanceSheetInverseLinkbase = balanceSheetLinkbase.reverse
#endif
	}

	subscript(itemID : DEGAAPItemID) -> ChartItem?
	{
#if CHART_ITEM_INCLUDES_LAW
		return _chartItemTree[itemID]?.item
#else
		return _chartItemTree[itemID]
#endif
	}

	subscript(itemName : String) -> ChartItem?
	{
		guard let id = DEGAAPItemID(itemName) else { return nil }
		return self[id]
	}

	/// - returns: Items whose ID or description contain the given `searchKey`.
	func items(describedBy searchKey : String, caseSensitive : Bool) -> [(DEGAAPItemID, String)]?
	{
		let matchingItems : [ChartItem] = {
			if caseSensitive
			{
#if CHART_ITEM_INCLUDES_LAW
				return _chartItemTree.find{ $0.item.id.name.contains(searchKey) || $0.item.definitionGerman.contains(searchKey) }.map{ $0.item }
#else
				return _chartItemTree.find{ $0.id.name.contains(searchKey) || $0.definitionGerman.contains(searchKey) }
#endif
			}
			else
			{
				let locale = Locale(identifier: "de_DE")
				let key = searchKey.lowercased(with: locale)
#if CHART_ITEM_INCLUDES_LAW
				return _chartItemTree.find{ $0.item.id.name.lowercased(with: locale).contains(key) || $0.item.definitionGerman.lowercased(with: locale).contains(key) }.map{ $0.item }
#else
				return _chartItemTree.find{ $0.id.name.lowercased(with: locale).contains(key) || $0.definitionGerman.lowercased(with: locale).contains(key) }
#endif
			}
		}()
		return matchingItems.map{ ($0.id, $0.definitionGerman) }
	}

	/// - returns: A list of ancestor accounts, from the highest level down to the given item.
	///
	/// If the given item was found in the chart, it will be included as the last item in the returned list.
	/// If the given item was not found, an empty list will be returned.
	func itemPath(for itemID : DEGAAPItemID) -> [DEGAAPItemID]
	{
		let incomeStatementPath = _incomeStatementInverseLinkbase.path(for: itemID)
		return !incomeStatementPath.isEmpty ? incomeStatementPath : _balanceSheetInverseLinkbase.path(for: itemID)
	}

	/// Returns the weights (-1, 0, +1) with which the given child accounts contribute to the given parent account.
	///
	/// For a child account to contribute positively or negatively to the given parent account,
	/// * the parent must be reachable from the child account in the inverse linkbase;
	/// * the periodType (duration or instant) must match;
	/// * parent and child must have the same balance type, i.e. matching credit/debit properties.
	func weights(for account : DEGAAPItemID, children childAccounts : [DEGAAPItemID], section : DEGAAPReportSection) -> [DEGAAPWeight]
	{
		switch section
		{
			case .incomeStatement : return _incomeStatementInverseLinkbase.weights(for: account, startingFrom: childAccounts)
			case .assets, .equityAndLiabilities : return _balanceSheetInverseLinkbase.weights(for: account, startingFrom: childAccounts)
			default : return []
		}
	}

	private static func _loadTaxonomyItemsFromFile(at fileLocation : URL) throws(DEGAAPError) -> [TaxonomyItem]
	{
		do {
			let decoder = CSVDecoder {
				$0.delimiters.field = ";"
				$0.delimiters.row = "\r\n"
				$0.headerStrategy = .firstLine
				$0.encoding = .utf8
				$0.nilStrategy = .empty
				$0.presample = false
				$0.decimalStrategy = .locale(Locale(languageCode: .german, script: .latin, languageRegion: .germany))
				$0.dataStrategy = .base64
				$0.bufferingStrategy = .sequential
				$0.trimStrategy = .whitespaces
				$0.escapingStrategy = .doubleQuote
			}
			return try decoder.decode([TaxonomyItem].self, from: fileLocation)
		} catch let error {
			throw DEGAAPError.taxonomyLoading("Could not decode DEGAAP chart items. \(error)")
		}
	}

	/// Converts the given taxonomy items into chart items.
	///
	/// Taxonomy items that are abstract or contain a value of a type other than _money_ will be rejected.
	private static func _createChartItems(from taxonomyItems : [TaxonomyItem]) -> Set<ExpandableChartItem>
	{
		var chartItems = Set<ExpandableChartItem>()
		var lastProcessedChartItem : DEGAAPItemID?
		for taxonomyItem in taxonomyItems
		{
			if let id = taxonomyItem.id
			{
				guard let isAbstract = taxonomyItem.abstract, isAbstract != .tiTrue,
					let valueType = taxonomyItem.valueType, valueType == .money,
					let tiReportSection = taxonomyItem.reportSection,
					let definitionDE = taxonomyItem.definitionDE,
					let definitionEN = taxonomyItem.definitionEN
					else { continue }
				let section = _reportSection(for: id, taxonomySection: tiReportSection)
				let chartItem = ChartItem(
					id: id,
					section: section,
					balanceType: taxonomyItem.balanceType,
					definition: definitionEN,
					definitionGerman: definitionDE,
					requirement: taxonomyItem.requirement
				)
#if CHART_ITEM_INCLUDES_LAW
				chartItems.insert(ExpandableChartItem(item: chartItem))
#else
				chartItems.insert(chartItem)
#endif
				lastProcessedChartItem = id
			}
#if CHART_ITEM_INCLUDES_LAW
			if taxonomyItem.id == nil,
			 let law = taxonomyItem.lawShortName,
			 let lastGaapID = lastProcessedChartItem
			{
				let newLaw = ChartItem.Law(name: law, paragraph: taxonomyItem.lawParagraph, subparagraph: taxonomyItem.lawSubparagraph)
				chartItems.first(where: { $0.item.id == lastGaapID})?.add(law: newLaw)
			}
#endif
		}
		return chartItems
	}

	private static func _reportSection(for chartItemID : DEGAAPItemID, taxonomySection : TaxonomyItem.TIReportSection) -> DEGAAPReportSection
	{
		if taxonomySection == .balanceSheet
		{
			guard chartItemID.path.count > 1, chartItemID.path[0] == "bs" else { return .none }
			if chartItemID.path[1] == "ass" { return .assets }
			else if chartItemID.path[1] == "eqLiab" { return .equityAndLiabilities }
			else { return .none }
		}
		else if taxonomySection == .noteOnBalanceSheet { return .notes }
		else if taxonomySection == .incomeStatement { return .incomeStatement }
		else if taxonomySection == .appendix { return .appendix }
		else if taxonomySection == .statusReport { return .statusReport }
		else { return .none }
	}

}
