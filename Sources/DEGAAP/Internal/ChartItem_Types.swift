//  Copyright 2020-2024 Kai Oezer

extension ChartItem
{
	enum BalanceType : String, Codable, Sendable
	{
		case none = ""
		case debit = "debit"
		case credit = "credit"
	}

	/// The role of the item (account) within the fiscal report transmitted to the tax authority.
	///
	/// German: Kennzeichnung der Rolle einer Bilanzposition (Konto) für den Geschäftsbericht.
	enum ReportingRequirement : String, Codable
	{
		/// These items must be present in any fiscal report.
		///
		/// Die Übermittlung aller als „Mussfeld“ gekennzeichneten Positionen ist (unabhängig von Rechtsform,
		/// dem Gewinnermittlungsverfahren, Branche o.ä.) als Mindestumfang des amtlich vorgeschriebenen
		/// Datensatzes im Sinne des [§ 5b EStG](https://www.gesetze-im-internet.de/estg/__5b.html)
		/// erforderlich. Es wird bei der Übermittlung an die Finanzverwaltung elektronisch geprüft, ob alle Mussfelder
		/// im Datensatz enthalten sind.
		case required = "Mussfeld"

		/// Parent items of `required` items that must be included in the fiscal report.
		///
		/// Oberpositionen von rechnerisch verknüpften Mussfeldern (`required`).
		/// Summenmussfelder müssen zusammen mit den Mussfeldern übermittelt werden.
		case requiredSummation = "Summenmussfeld"

		/// Items with this role are required to be included in the fiscal report only if they
		/// contribute to the calculation of a `requiredSummation`.
		///
		/// Positionen, die auf der gleichen Ebene wie rechnerisch verknüpfte Mussfelder stehen.
		/// Diese Positionen sind dann zwingend mit Werten zu übermitteln, wenn ohne diese
		/// Übermittlung die Summe der Positionen auf der gleichen Ebene einer Rechenregel
		/// nicht dem Wert der Oberposition entspricht.
		case requiredIfContributing = "Rechnerisch notwendig, soweit vorhanden"

		/// This is a `required` item, for which a listing of the items contributing to its total
		/// needs to be transmitted as well.
		///
		/// Bei diesen Positionen der Taxonomie ist zusätzlich zur Rolle als Mussfeld erforderlich,
		/// dass der Auszug aus der Summen-/Saldenliste der in diese Position einfließenden Konten
		/// im XBRL-Format mitverschickt wird.
		case requiredWithIndividualAccounts = "Mussfeld, Kontennachweis erwünscht"
	}
	
#if CHART_ITEM_INCLUDES_LAW
	struct Law : Codable, Hashable, CustomStringConvertible, Sendable
	{
		let name : String
		let paragraph : String?
		let subparagraph : String?

		var description: String { name
			+ (paragraph != nil ? (" § " + paragraph!) : "")
			+ (subparagraph != nil ? (" Abs. " + subparagraph!) : "")
		}
	}
#endif
}
