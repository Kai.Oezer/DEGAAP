//  Copyright 2025 Kai Oezer

import Foundation
import RegexBuilder

public struct DEGAAPSourceChartGroup : Identifiable, CustomStringConvertible, Comparable, Equatable
{
	public var range : ClosedRange<DEGAAPSourceChartItemID>
	public var description : String

	public var id : DEGAAPSourceChartItemID { range.lowerBound }

	public static func < (lhs: DEGAAPSourceChartGroup, rhs: DEGAAPSourceChartGroup) -> Bool
	{
		lhs.range.upperBound < rhs.range.lowerBound
	}

	public static func > (lhs: DEGAAPSourceChartGroup, rhs: DEGAAPSourceChartGroup) -> Bool
	{
		lhs.range.lowerBound > rhs.range.upperBound
	}

	public static func == (lhs: DEGAAPSourceChartGroup, rhs: DEGAAPSourceChartGroup) -> Bool
	{
		lhs.range == rhs.range
	}
}

public struct DEGAAPSourceChartCategory : Identifiable, CustomStringConvertible, Comparable, Equatable
{
	public var id : Int
	public var description : String
	public var groups = [DEGAAPSourceChartGroup]()

	public static func < (lhs: DEGAAPSourceChartCategory, rhs: DEGAAPSourceChartCategory) -> Bool
	{
		lhs.id < rhs.id
	}

	public static func == (lhs: DEGAAPSourceChartCategory, rhs: DEGAAPSourceChartCategory) -> Bool
	{
		lhs.id == rhs.id
	}
}

func loadSourceChartGroups(for chart : DEGAAPSourceChart) -> [DEGAAPSourceChartCategory]?
{
#if false
	let accountCategoryPattern = /^(\d)xxx\s+(.+)\s*$/
	let accountGroupPattern = /^(\d{4})-(\d{4})\s+(.+)\s*$/
#else
	let accountCategoryPattern = Regex {
		Anchor.startOfLine
		Capture {
			CharacterClass.digit
		}
		"xxx"
		OneOrMore {
			CharacterClass.whitespace
		}
		Capture {
			OneOrMore {
				CharacterClass.any
			}
		}
		ZeroOrMore {
			CharacterClass.whitespace
		}
		Anchor.endOfLine
	}
	let accountGroupPattern = Regex {
		Anchor.startOfLine
		Capture {
			Repeat(count: 4) {
				CharacterClass.digit
			}
		}
		"-"
		Capture {
			Repeat(count: 4) {
				CharacterClass.digit
			}
		}
		OneOrMore {
			CharacterClass.whitespace
		}
		Capture {
			OneOrMore {
				CharacterClass.any
			}
		}
		ZeroOrMore {
			CharacterClass.whitespace
		}
		Anchor.endOfLine
	}
#endif

	guard let chartGroupsDataLocation = Bundle.module.url(forResource: "SourceChartGroups-\(chart)", withExtension: "txt", subdirectory: "Resources"),
		let fileContent = try? String(contentsOf: chartGroupsDataLocation, encoding: .utf8)
		else {
			assertionFailure("Could not load categories and groups data for source chart \(chart).")
			return nil
		}
	let chartGroupsDataLines = fileContent.components(separatedBy: .newlines)
	var result = [DEGAAPSourceChartCategory]()
	var currentCategory : DEGAAPSourceChartCategory? = nil

	for line in chartGroupsDataLines {
		if let (_, categoryIDString, categoryDescription) = line.wholeMatch(of: accountCategoryPattern)?.output, let categoryID = DEGAAPSourceChartCategoryID(categoryIDString) {
			if let currentCategory {
				result.append(currentCategory)
			}
			currentCategory = .init(id: categoryID, description: String(categoryDescription))
		} else if let (_, groupStartIDString, groupEndIDString, groupDescription) = line.wholeMatch(of: accountGroupPattern)?.output,
			let groupStartID = DEGAAPSourceChartItemID(String(groupStartIDString)),
			let groupEndID = DEGAAPSourceChartItemID(String(groupEndIDString)) {
			assert(currentCategory != nil)
			if currentCategory != nil {
				assert(groupStartID.accountCategory == groupEndID.accountCategory)
				assert(currentCategory!.id == groupStartID.accountCategory)
				let groupRange = groupStartID...groupEndID
				assert(currentCategory!.groups.first(where: {$0.range.overlaps(groupRange)}) == nil)
				let group = DEGAAPSourceChartGroup(range: groupRange, description: String(groupDescription))
				currentCategory!.groups.append(group)
			}
		}
	}
	if let currentCategory {
		result.append(currentCategory)
	}
	return result
}
