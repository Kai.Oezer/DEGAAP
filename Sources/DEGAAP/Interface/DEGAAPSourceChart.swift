// Copyright 2021-2025 Kai Oezer

import Foundation

/// One of the popular accounting charts used in Germany, where individual accounts are identified by a four-digit code.
public enum DEGAAPSourceChart : String, Hashable, Codable, Sendable, CustomStringConvertible, CaseIterable
{
	case ikr    = "IKR"     /// Industriekontenrahmen
	case skr03  = "SKR03"   /// DATEV SKR 03
	case skr04  = "SKR04"   /// DATEV SKR 04

	public var description : String { rawValue }
}

#if false
extension DEGAAPSourceChart
{
	public func reportSection(for itemID : DEGAAPSourceChartItemID) -> DEGAAPReportSection?
	{
		return nil
	}
}
#endif

extension DEGAAPSourceChart
{
	public func formatted<T : FormatStyle>(_ formatStyle : T) -> String where T.FormatInput == DEGAAPSourceChart, T.FormatOutput == String
	{
		formatStyle.format(self)
	}

	public init<T : ParseStrategy>(_ value : String, strategy : T) throws where T.ParseInput == String, T.ParseOutput == DEGAAPSourceChart
	{
		self = try strategy.parse(value)
	}
}
