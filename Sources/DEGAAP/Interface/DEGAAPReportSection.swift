// Copyright 2021-2024 Kai Oezer

/// The sections of a business report for very small, limited liability corporations in Germany.
///
/// The same account can be used in multiple report sections. For example, the accounts
/// for fixed assets, which are part of the balance sheet and the inventory.
///
/// The choice of `Int` as the raw value, instead of `String`, should make search operations faster.
public enum DEGAAPReportSection : Int, Codable, Sendable
{
	case none = 0

	/// Bilanz Aktiva
	case assets = 10

	/// Bilanz Passiva
	case equityAndLiabilities = 20

	/// Angaben unter der Bilanz
	case notes = 30

	/// GuV
	case incomeStatement = 40

	/// Ergebnisverwendung
	case incomeUse = 50

	/// Eigenkapitalspiegel (für Konzerne)
	// case equityChangeOfGroup = 60

	/// Anhang
	case appendix = 70

	/// Anlagenspiegel
	case assetChangeStatement = 75

	/// Lagebericht
	case statusReport = 90
}
